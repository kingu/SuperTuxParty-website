from app import app, API_TOKEN
from flask import render_template, send_from_directory, abort, request, redirect

from pathlib import Path

import os

def render_markdown(source_file):
    with open(source_file, 'r') as f:
        return render_template('markdown.html', content=f.read())

def get_blog_entries(offset = None, limit = None):
    files = ['blog/' + x for x in os.listdir('blog') if not x.endswith('.hidden')]
    files.sort(key=os.path.getmtime, reverse=True) # Only return the newest elements

    if offset and limit:
        files = files[offset:offset+limit]
    elif offset:
        files = files[offset:]
    
    return [open(f, 'r').read() for f in files]

@app.route('/')
def index():
    return render_template('index.html', posts=get_blog_entries(offset=0, limit=10)) # Display the ten most recent entries

@app.route('/blog')
def blog():
    return render_template('index.html', posts=get_blog_entries())

@app.route('/download')
def download_redirect():
    return redirect('/download/latest')

@app.route('/download/<name>')
def download(name):
    path = Path('downloads/').joinpath(name)
    try:
        path.relative_to('downloads/')
    except:
        abort(400)
    
    if not path.exists():
        abort(404)
    
    return render_template('download.html', release=name, version=path.resolve().name)

@app.route('/download/<name>/<os>')
def download_file(name, os):
    return send_from_directory('downloads', name + "/" + os + ".zip", as_attachment=True, attachment_filename="super-tux-party-{}-{}.zip".format(os, name))

@app.route('/upload/<name>/<os>', methods=['POST'])
def upload(name, os):
    api_token = request.headers.get('X-API-Token')
    if api_token != API_TOKEN:
        abort(403)

    if name == 'latest':
        abort(400)

    path = Path('downloads/').joinpath(name)
    try:
        path.relative_to('downloads/')
    except:
        abort(400)

    if not path.exists():
        path.mkdir()
    path = path.joinpath(os + ".zip")

    try:
        path.relative_to('downloads/')
    except:
        abort(400)

    with open(path, 'wb') as f:
        f.write(request.data)
    
    return '', 201

@app.route('/source')
def source():
    return render_markdown("markdown/source.md")

@app.route('/contact')
def contact():
    return render_markdown("markdown/contact.md")
    
