from flask import Flask
from flaskext.markdown import Markdown

API_TOKEN = open('token', 'r').read()

app = Flask(__name__, static_url_path='', static_folder='static/')
markdown = Markdown(app)

application = app # You happy now uWSGI???

import routes
