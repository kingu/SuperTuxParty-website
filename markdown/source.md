# Source Code

The project is hosted on [gitlab](https://gitlab.com/supertuxparty/supertuxparty)

You can download it via git:

```
git clone https://gitlab.com/supertuxparty/supertuxparty.git
cd supertuxparty
```
