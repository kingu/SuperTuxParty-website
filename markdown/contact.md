# Contact

The best way of contacting us is through one of these channels:

## Gitlab
Whenever you've found a bug or got an idea for improvement, please add them to our [issue tracker](https://gitlab.com/supertuxparty/supertuxparty/issues).

## Matrix
Most communication happens on [Matrix](https://matrix.org), a free messaging protocol.

Join the channel [#SuperTuxParty-Extra:matrix.org](https://matrix.to/#/#SuperTuxParty-Extra:matrix.org) for general discussion of the project.

We also have [#SuperTuxParty-Dev:matrx.org](https://matrix.to/#/#SuperTuxParty-Dev:matrix.org) for discussion regarding development.

## Reddit
If you're more familiar with Reddit than Matrix, you can create a post on our [subreddit](https://reddit.com/r/supertuxparty)

---
# Responsible for content on this site
**Name**: Florian Kothmeier

<div style="display: inline;">
	<strong>Address</strong>:
	<p style="display: inline-block; vertical-align: top; margin: 0;">
		Fahrstraße 6,<br>
		91054 Erlangen, Germany
	</p>
</div>

**Email**: [info@supertux.party](mailto:info@supertux.party)
